import {Injectable} from '@angular/core';
import {SQLite, SQLiteObject} from '@ionic-native/sqlite';
import {Customer} from "../../models/customer/customer.module";
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {Platform} from "ionic-angular";
import {Invoice} from "../../models/invoice/invoice.module";

/*
  Generated class for the DatabaseProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class DatabaseProvider {

  private database: SQLiteObject;
  private dbReady = new BehaviorSubject<boolean>(false);

  constructor(private platform: Platform, private sqLite: SQLite) {
    console.log('constructor');
    this.platform.ready().then(() => {
      console.log('platform ready');
      this.sqLite.create({name: 'data.db', location: 'default'})
        .then((db: SQLiteObject) => {
          this.database = db;
          this.createTables()
            .then(() => console.log('executed sql'))
            .catch(e => console.log(e));
          this.dbReady.next(true);
        }).catch(e => console.log(e))
    }).catch(err => {
      console.log('platform not ready: ', err);
    })
  }

  private createTables() {
    return this.database.executeSql(
      `CREATE TABLE IF NOT EXISTS customers (
      id        INTEGER PRIMARY KEY AUTOINCREMENT, 
      fullName  TEXT NOT NULL,  
      nit       TEXT NOT NULL,
      selected  NUMERIC DEFAULT 0
      );`, {})
      .then(() => {
        console.log('customers table created!');
        return this.database.executeSql(
          `CREATE TABLE IF NOT EXISTS invoices (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        date          TEXT,
        nit           TEXT,
        businessName  TEXT,
        invoiceNumber TEXT,
        policyNumber  TEXT,
        authNumber    TEXT,
        totalBill     TEXT,
        exempt        TEXT,
        subTotalBill  TEXT,
        discount      TEXT,
        baseImp       TEXT,
        cf            TEXT,
        controlCode   TEXT,
        compType      TEXT,
        monthNumber   INTEGER,
        customerId    INTEGER
        );`, {})
          .then(() => console.log('invoices table created!'))
          .catch((err) => console.log('error: invoices table created', err))
      })
      .catch((err) => {
        console.log('error detected creating customer table', err);
      })
  }

  private isReady() {
    return new Promise((resolve, reject) => {
      if (this.dbReady.getValue())
        resolve();
      else {
        this.dbReady.subscribe((ready) => {
          if (ready)
            resolve();
        });
      }
    })
  }

  createCustomer(customer: Customer) {
    return this.database.executeSql(`INSERT INTO customers (fullName, nit, selected ) VALUES (?, ?, ?);`,
      [customer.fullName, customer.nit, customer.selected])
      .then(resp => {
        if (resp.insertId)
          return Promise.resolve(this.getCustomerById(resp.insertId));
      })
      .catch(err => console.log('INSERT err', err));
  }

  createInvoice(invoice: Invoice) {
    return this.database.executeSql(`INSERT INTO invoices 
        (date, nit, businessName, invoiceNumber, policyNumber, authNumber, 
        totalBill, exempt, subTotalBill, discount, baseImp, cf, controlCode, 
        compType, monthNumber, customerId) 
        VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);`,
      [invoice.date,
        invoice.nit,
        invoice.businessName,
        invoice.invoiceNumber,
        invoice.policyNumber,
        invoice.authNumber,
        invoice.totalBill,
        invoice.exempt,
        invoice.subTotalBill,
        invoice.discount,
        invoice.baseImp,
        invoice.cf,
        invoice.controlCode,
        invoice.compType,
        invoice.monthNumber,
        invoice.customerId])
      .then(resp => {
        if (resp.insertId)
          return Promise.resolve(this.getInvoiceById(resp.insertId));
      }).catch(err => console.log('INSERT invoice err', err));

  }

  updateCustomer(customer: Customer) {
    return this.isReady()
      .then(() => {
        return this.database.executeSql(`UPDATE customers SET (fullName, nit, selected) = (?,?,?) WHERE id = ?`,
          [customer.fullName, customer.nit, customer.selected, customer.id])
          .then(resp => {
            console.log('UPDATED ', resp);
          });
      });
  }

  getCustomerById(id: number) {
    return this.isReady()
      .then(() => {
        return this.database.executeSql(`SELECT * FROM customers WHERE id = ${id}`, [])
          .then((data) => {
            if (data.rows.length) {
              return data.rows.item(0);
            }
            return null;
          })
      })
  }

  getInvoiceById(id: number) {
    return this.isReady()
      .then(() => {
        return this.database.executeSql(`SELECT * FROM invoices WHERE id = ${id}`, [])
          .then((data) => {
            if (data.rows.length) {
              return data.rows.item(0);
            }
            return null;
          })
      })
  }

  deleteCustomer(customer: Customer) {
    return this.isReady()
      .then(() => {
        return this.database.executeSql(`DELETE FROM customers WHERE id = ?`, [customer.id])
      })
  }

  deleteAllInvoices() {
    return this.isReady()
      .then(() => {
        return this.database.executeSql(`DELETE FROM invoices`, [])
      })
  }

  dropCustomerTable() {
    return this.isReady()
      .then(() => {
        return this.database.executeSql(`DROP TABLE customers`, [])
      })
  }

  deleteInvoicesByCustomerId(customerId: number) {
    return this.isReady()
      .then(() => {
        return this.database.executeSql(`DELETE FROM invoices WHERE customerId = ?`, [customerId])
      })
  }

  deleteInvoicesByMonthNumber(monthNumber: number) {
    return this.isReady()
      .then(() => {
        return this.database.executeSql(`DELETE FROM invoices WHERE monthNumber = ?`, [monthNumber])
      })
  }

  deleteAllCustomers() {
    return this.isReady()
      .then(() => {
        return this.database.executeSql(`DELETE FROM customers`, [])
      })
  }

  getAllCustomers() {
    return this.database.executeSql(`SELECT * FROM customers ORDER BY fullName ASC `, [])
      .then((data) => {
        let customers: Array<Customer> = [];
        for (let i = 0; i < data.rows.length; i++) {
          customers.push({
            id: data.rows.item(i).id,
            fullName: data.rows.item(i).fullName,
            nit: data.rows.item(i).nit,
            selected: data.rows.item(i).selected
          });
        }
        this.dbReady.next(true);
        return Promise.resolve(customers);
      })
      .catch(e => Promise.reject(e));
  }

  getInvoiceByCustomerIdAndMonthNumber(customerId: number, monthNumber: number) {
    return this.database.executeSql(`SELECT * FROM invoices WHERE customerId = ? AND monthNumber = ?`,
      [customerId, monthNumber])
      .then((data) => {
        let invoices: Array<Invoice> = [];
        for (let i = 0; i < data.rows.length; i++) {
          invoices.push({
            id: data.rows.item(i).id,
            date: data.rows.item(i).date,
            nit: data.rows.item(i).nit,
            businessName: data.rows.item(i).businessName,
            invoiceNumber: data.rows.item(i).invoiceNumber,
            policyNumber: data.rows.item(i).policyNumber,
            authNumber: data.rows.item(i).authNumber,
            totalBill: data.rows.item(i).totalBill,
            exempt: data.rows.item(i).exempt,
            subTotalBill: data.rows.item(i).subTotalBill,
            discount: data.rows.item(i).discount,
            baseImp: data.rows.item(i).baseImp,
            cf: data.rows.item(i).cf,
            controlCode: data.rows.item(i).controlCode,
            compType: data.rows.item(i).compType
          });
        }
        this.dbReady.next(true);
        return Promise.resolve(invoices);
      })
      .catch(e => Promise.reject(e));
  }

  getInvoiceByControlCode(controlCode: string) {
    return this.database.executeSql(`SELECT * FROM invoices WHERE controlCode = ?`,
      [controlCode])
      .then((data) => {
        let invoice: Invoice = null;
        if (data.rows.length)
          invoice = data.rows.item(0);
        this.dbReady.next(true);
        return Promise.resolve(invoice);
      })
      .catch(e => Promise.reject(e));
  }

  getCustomerSelected() {
    return this.database.executeSql(`SELECT * FROM customers WHERE selected = ?`,
      [true])
      .then((data) => {
        let customer: Customer = null;
        if (data.rows.length)
          customer = data.rows.item(0);
        this.dbReady.next(true);
        return Promise.resolve(customer);
      })
      .catch(e => Promise.reject(e));
  }

  getCustomerByNameAndNit(customer: Customer) {
    return this.database.executeSql(`SELECT * FROM customers WHERE fullName = ? AND nit = ?`,
      [customer.fullName, customer.nit])
      .then((data) => {
        let customer: Customer = null;
        if (data.rows.length)
          customer = data.rows.item(0);
        this.dbReady.next(true);
        return Promise.resolve(customer);
      }).catch(e => Promise.reject(e));
  }
}
