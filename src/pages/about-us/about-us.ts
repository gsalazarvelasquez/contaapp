import {Component} from '@angular/core';
import {IonicPage, NavParams} from 'ionic-angular';
import {EmailComposer} from "@ionic-native/email-composer";
import {Email} from "../../models/email/email.module";
import {AppVersion} from "@ionic-native/app-version";

/**
 * Generated class for the AboutUsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-about-us',
  templateUrl: 'about-us.html',
})
export class AboutUsPage {

  myMail: string;
  appVersionNumber: string;

  constructor(public navParams: NavParams,
              public  appVersion: AppVersion,
              private emailComposer: EmailComposer) {
    this.myMail = "gsalazarvelasquez@gmail.com";
    this.appVersionNumber = '0.0.0';

    this.appVersion.getVersionCode().then(resp => {
      this.appVersionNumber = resp;
    })
  }

  sendMail() {
    let email: Email = {
      to: this.myMail,
      subject: `Consulta`,
      isHtml: true
    };
    this.emailComposer.isAvailable().then((available) => {
      console.log('isAvailable: ', available);
      this.emailComposer.open(email);
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AboutUsPage');
  }

}
