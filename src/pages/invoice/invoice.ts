import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {BarcodeScanner} from '@ionic-native/barcode-scanner';
import {Invoice} from "../../models/invoice/invoice.module";
import {Customer} from "../../models/customer/customer.module";
import {AlertService} from "../../services/alert/alert.service";
import {DatabaseProvider} from "../../providers/database/database";
import {ToastService} from "../../services/toast/toast.service";
import {Email} from "../../models/email/email.module";
import {File} from "@ionic-native/file";
import {EmailComposer} from "@ionic-native/email-composer";

/**
 * Generated class for the InvoicePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-invoice',
  templateUrl: 'invoice.html',
})
export class InvoicePage {

  scannedCode: string;
  invoiceData: Array<string>;
  invoice: Invoice;
  customer: Customer;
  date: String;
  customers: Array<Customer>;
  monthNumber: number;
  csvData: string;
  fileName: string;
  fileFormat: string;
  invoices: Array<Invoice>;
  captures: number;
  passValidation: boolean;

  constructor(public navParams: NavParams,
              public navCtrl: NavController,
              public scanner: BarcodeScanner,
              public alert: AlertService,
              private database: DatabaseProvider,
              private toast: ToastService,
              private file: File,
              private emailComposer: EmailComposer) {
    this.passValidation = true;
    this.invoice = {
      nit: '',
      invoiceNumber: '',
      authNumber: '',
      date: '',
      controlCode: '',
      discount: '',
      totalBill: '',
      exempt: '',
      subTotalBill: '',
      baseImp: '',
      cf: '',
      compType: "1",
      policyNumber: ''
    };
    this.invoices = [];
    this.customer = {fullName: '', nit: '', id: 0};
    this.customers = [];
    this.scannedCode = null;
    this.csvData = '';
    this.initializeMonth();
    this.fileName = new Date().toISOString();
    this.fileFormat = '.txt';
    this.captures = 0;
  }

  sendViaMailInvoices() {
    if (this.captures == 0) {
      this.alert.show('Información', `El mes seleccionado tiene 0 capturas`);
    } else {
      this.convertInvoiceToCsv();
      this.writeFileAndSendMail();
    }
  }

  monthChanges() {
    let arrayDate: Array<string> = this.date.split("-");
    this.monthNumber = parseInt(arrayDate[1]);
    this.getInvoices();
  }

  scanCodeAndSaveInvoice() {
    this.scanner.scan().then(barcodeResp => {
      if (!barcodeResp.cancelled) {
        this.scannedCode = barcodeResp.text;
        this.invoiceData = this.scannedCode.split("|");
        if (this.validateDataInvoice()) {
          this.orderInvoiceData();
          this.saveInvoice();
        }
      }
    })
  }

  saveInvoice() {
    console.log('save invoice: ', this.invoice);
    this.isDuplicateInvoice().then((resp) => {
      if (resp)
        this.alert.show('Información', `Esta factura ya se encuentra registrada`, 'Aceptar');
      else {
        this.database.createInvoice(this.invoice).then((data) => {
            this.toast.show(`Factura Guardada!`);
            this.getInvoices();
          }, (err) => console.log('createInvoice err', err)
        ).catch(err => console.log('Create invoice err:', err))
      }
    })
  }

  private orderInvoiceData(): Invoice {
    console.log('invoice data: ', this.invoiceData);
    this.invoice.nit = this.invoiceData[0];
    this.invoice.invoiceNumber = this.invoiceData[1];
    this.invoice.authNumber = this.invoiceData[2];
    this.invoice.date = this.invoiceData[3];
    this.invoice.controlCode = this.invoiceData[6];
    this.invoice.discount = this.invoiceData[11];
    this.invoice.totalBill = this.calTotalBill();
    this.invoice.exempt = this.calExempt();
    this.invoice.subTotalBill = this.calSubtotalBill();
    this.invoice.baseImp = this.calBaseImp();
    this.invoice.cf = this.calCF();
    this.invoice.policyNumber = "0";
    this.invoice.monthNumber = this.monthNumber;
    this.invoice.customerId = this.customer.id;
    return this.invoice;
  }

  private calTotalBill(): string {
    return (parseFloat(this.invoiceData[4]) - parseFloat(this.invoiceData[11])).toString();
  }

  private calExempt(): string {
    return this.invoiceData[4] === this.invoiceData[5] ? "0.00" : (parseFloat(this.invoiceData[4]) - parseFloat(this.invoiceData[5])).toString();
  }

  private calSubtotalBill(): string {
    return (parseFloat(this.calTotalBill()) - parseFloat(this.calExempt())).toString();
  }

  private calBaseImp(): string {
    return (parseFloat(this.calSubtotalBill()) - parseFloat(this.invoiceData[11])).toString();
  }

  private calCF(): string {
    return (parseFloat(this.calSubtotalBill()) * 13 / 100).toFixed(2);
  }

  private validateDataInvoice(): boolean {
    let flag = true;
    if (this.isInvoiceNitValid()) {
      this.alert.show('Información',
        `El NIT/CI de captura: ${this.invoiceData[7]} no corresponde al NIT/CI: ${this.customer.nit}`,
        'Aceptar');
      flag = false;
    }
    else if (this.isInvoiceDateValid()) {
      this.alert.show('Información',
        `El Fecha de captura: ${this.invoiceData[3]} no corresponde al Mes: ${this.monthNumber}`,
        'Aceptar');
      flag = false;
    }
    return flag;
  }

  private isDuplicateInvoice() {
    return this.database.getInvoiceByControlCode(this.invoiceData[6]).then(resp => {
      return resp != null;
    });
  }

  private isInvoiceNitValid(): boolean {
    return this.customer.nit != this.invoiceData[7];
  }

  private isInvoiceDateValid(): boolean {
    let date = this.invoiceData[3].split('/');
    return this.monthNumber != parseInt(date[1]);
  }

  private writeFileAndSendMail() {
    this.fileName = this.customer.fullName + '-' + this.monthNumber;
    this.file.writeFile(this.file.dataDirectory, this.fileName + this.fileFormat, this.csvData, {replace: true})
      .then(() => {
        let email: Email = {
          attachments: [this.file.dataDirectory + this.fileName + this.fileFormat],
          subject: `Facturas Cliente: ${this.customer.fullName} | Mes ${this.monthNumber}`,
          body: `Se adjunta las facturas del cliente: <b>${this.customer.fullName}</b> del mes de: <b>${this.monthNumber}</b> `,
          isHtml: true
        };
        this.emailComposer.isAvailable().then((available) => {
          console.log('isAvailable: ', available);
          this.emailComposer.open(email);
          this.removeFile();
        });
      }).catch((err) => console.error('error when write file: ', err));
  }

  private removeFile() {
    this.file.removeFile(this.file.dataDirectory, this.fileName + this.fileFormat).then((resp) => {
      console.log('file removed! ', resp);
    }).catch(err => console.log('err when remove file: ', err));
  }

  private convertInvoiceToCsv() {
    for (let i = 0; i < this.invoices.length; i++) {
      this.csvData += '1|';
      this.csvData += (i + 1) + '|';
      this.csvData += this.invoices[i].date + '|';
      this.csvData += this.invoices[i].nit + '|';
      this.csvData += this.invoices[i].businessName + '|';
      this.csvData += this.invoices[i].invoiceNumber + '|';
      this.csvData += this.invoices[i].policyNumber + '|';
      this.csvData += this.invoices[i].authNumber + '|';
      this.csvData += this.invoices[i].totalBill + '|';
      this.csvData += this.invoices[i].exempt + '|';
      this.csvData += this.invoices[i].subTotalBill + '|';
      this.csvData += this.invoices[i].discount + '|';
      this.csvData += this.invoices[i].baseImp + '|';
      this.csvData += this.invoices[i].cf + '|';
      this.csvData += this.invoices[i].controlCode + '|';
      this.csvData += this.invoices[i].compType;
      this.csvData += '\n';
    }
  }

  private getCustomerSelectedAndInvoices() {
    // this.customer = {id: 0, fullName: 'Salazar', nit: '6425177'};
    this.database.getCustomerSelected().then((customer: Customer) => {
      if (customer == null) {
        this.alert.show('Información',
          `Debe tener un cliente agregado por lo menos`,
          'Aceptar');
        this.navCtrl.parent.select(0);
      } else {
        this.customer = customer;
        this.getInvoices();
      }
    })
  }

  private initializeMonth() {
    this.date = new Date().toISOString();
    let arrayDate: Array<string> = this.date.split("-");
    this.monthNumber = parseInt(arrayDate[1]);
  }

  private getInvoices() {
    // TODO: Comment to make local test | Uncomment to production environment
    this.database.getInvoiceByCustomerIdAndMonthNumber(this.customer.id, this.monthNumber)
      .then((invoices: Array<Invoice>) => {
        console.log('get invoices: ', invoices);
        this.invoices = invoices;
        this.captures = this.invoices.length;
      });
  }

  ionViewWillEnter() {
    this.getCustomerSelectedAndInvoices();
  }
}
