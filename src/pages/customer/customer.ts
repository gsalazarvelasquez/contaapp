import {Component} from '@angular/core';
import {AlertController, IonicPage, ItemSliding, Modal, ModalController, NavParams} from 'ionic-angular';
import {Customer} from "../../models/customer/customer.module";
import {DatabaseProvider} from "../../providers/database/database";
import {ToastService} from "../../services/toast/toast.service";

/**
 * Generated class for the CustomerPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-customer',
  templateUrl: 'customer.html',
})
export class CustomerPage {

  customers: Array<Customer>;

  constructor(private modal: ModalController,
              public navParams: NavParams,
              private alertCtrl: AlertController,
              private database: DatabaseProvider,
              private toast: ToastService) {
    this.customers = [];
  }

  onSelectCustomerAndUpdate(index) {
    for (let i = 0; i < this.customers.length; i++) {
      this.customers[i].selected = index === i;
      this.database.updateCustomer(this.customers[i]).then(resp => console.log('resp', resp));
    }
  }

  addCustomerModal() {
    const myModal: Modal = this.modal.create('ModalPage');
    myModal.onDidDismiss((customer: Customer) => {
      console.log('customer added: ', customer);
      this.getAllCustomers();
    });
    myModal.present();
  }

  editCustomer(customer: Customer, slidingItem: ItemSliding) {
    slidingItem.close();
    const myModal: Modal = this.modal.create('ModalPage', {customer: customer});
    myModal.onDidDismiss(() => {
      this.getAllCustomers();
    });
    myModal.present();
  }

  removeCustomer(customer: Customer, slidingItem: ItemSliding) {

    let alert = this.alertCtrl.create({
      title: 'Eliminar',
      message: `Desea eliminar al cliente ${customer.fullName}? Se eliminara todas las facturas de este cliente.`,
      buttons: [{
        text: 'Cancelar',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }, {
        text: 'Aceptar',
        handler: () => {
          this.database.deleteInvoicesByCustomerId(customer.id).then(resp => {
            this.database.deleteCustomer(customer).then((resp) => {
              console.log('deleteCustomer resp: ', resp);
              this.toast.show(`${customer.fullName} Eliminado!`);
              this.getAllCustomers();
            })
          })
        }
      }]
    });
    slidingItem.close();
    alert.present();
  }

  private getAllCustomers() {
    // TODO: comment database to work locally |
    // this.customers = [
    //   {id: 1, fullName: 'Salazar', nit: '6425177', selected: true},
    //   {id: 2, fullName: 'Belen', nit: '7918446', selected: false},
    //   {id: 3, fullName: 'Gonzalo', nit: '7918446', selected: false},
    // ];
    this.database.getAllCustomers().then((customers: Array<Customer>) => this.customers = customers);
  }

  ionViewWillEnter() {
    this.getAllCustomers();
  }
}
