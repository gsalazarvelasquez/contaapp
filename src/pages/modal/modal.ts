import {Component} from '@angular/core';
import {IonicPage, NavParams, ViewController} from 'ionic-angular';
import {Customer} from "../../models/customer/customer.module";
import {DatabaseProvider} from "../../providers/database/database";
import {AlertService} from "../../services/alert/alert.service";

@IonicPage()
@Component({
  selector: 'page-modal',
  templateUrl: 'modal.html',
})
export class ModalPage {
  customer: Customer;
  isDataValid: boolean;

  constructor(public navParams: NavParams,
              private view: ViewController,
              private database: DatabaseProvider,
              private alert: AlertService) {

    this.isDataValid = false;
    this.customer = {fullName: '', nit: '', selected: false};
  }

  ionViewDidLoad() {
    this.customer = this.navParams.get('customer') === undefined ? this.customer : this.navParams.get('customer');
  }

  cancel() {
    this.view.dismiss();
  }

  saveCustomer() {
    this.customer.id ? this.updateCustomer() : this.addCustomer();

  }

  private updateCustomer() {
    this.database.getCustomerByNameAndNit(this.customer).then(resp => {
      let customerNotExist: boolean = resp === null ? true : resp.id !== this.customer.id;
      if (customerNotExist) {
        this.database.updateCustomer(this.customer).then(() => this.view.dismiss(),
          (err) => {
            console.log('Update customer err', err);
          }).catch(err => console.log('Update customer err:', err));
      } else {
        this.alert.show('Información',
          `Ya existe un perfil con el Nombre: ${this.customer.fullName}  y el NIT/CI: ${this.customer.nit}`);
      }
    })
  }

  private addCustomer() {
    this.database.getAllCustomers().then((resp) => {
        this.customer.selected = resp.length === 0;
        this.database.getCustomerByNameAndNit(this.customer).then(resp => {
          if (resp === null) {
            this.database.createCustomer(this.customer).then(() => this.view.dismiss(),
              (err) => {
                console.log('err', err);
              }).catch(err => console.log('Create customer err:', err))
          } else {
            this.alert.show('Información',
              `Ya existe un perfil con el Nombre: ${this.customer.fullName}  y el NIT/CI: ${this.customer.nit}`);
          }
        })
      }
    );
  }

  onChangeInputs() {
    this.customer.fullName = this.customer.fullName.trim();
    this.customer.nit = this.customer.nit.trim();
    console.log('onChangeInputs: ', this.customer);

    this.isDataValid =
      this.customer.fullName != null &&
      this.customer.fullName != '' &&
      this.customer.nit != null &&
      this.customer.nit != '';
  }
}
