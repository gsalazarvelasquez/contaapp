export interface Email {
  to?: string,
  cc?: string,
  bcc?: Array<string>,
  attachments?: Array<string>,
  subject?: string,
  body?: string,
  isHtml?: boolean
}
