export interface Customer {
  key?: string,
  id?: number,
  fullName: string,
  nit: string,
  selected?: boolean,
}
