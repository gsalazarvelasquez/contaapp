import {BrowserModule} from '@angular/platform-browser';
import {ErrorHandler, NgModule} from '@angular/core';
import {IonicApp, IonicErrorHandler, IonicModule} from 'ionic-angular';
import {SplashScreen} from '@ionic-native/splash-screen';
import {StatusBar} from '@ionic-native/status-bar';

import {MyApp} from './app.component';
import {ToastService} from "../services/toast/toast.service";
import {BarcodeScanner} from "@ionic-native/barcode-scanner";

import {DatabaseProvider} from '../providers/database/database';
import {SQLite} from "@ionic-native/sqlite";
import {EmailComposer} from "@ionic-native/email-composer";
import {File} from '@ionic-native/file';
import {AlertService} from "../services/alert/alert.service";
import {AppVersion} from "@ionic-native/app-version";


@NgModule({
  declarations: [
    MyApp
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp
  ],
  providers: [
    StatusBar,
    SplashScreen,
    ToastService,
    BarcodeScanner,
    AlertService,
    SQLite,
    DatabaseProvider,
    EmailComposer,
    File,
    AppVersion,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
  ]
})
export class AppModule {
}
