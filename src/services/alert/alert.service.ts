import {Injectable} from "@angular/core";
import {AlertController} from "ionic-angular";

@Injectable()
export class AlertService {
  constructor(public alertCtrl: AlertController) {
  }

  show(title: string, subTitle: string, buttonMsg: string = "Ok") {
    return this.alertCtrl.create({
      title: title,
      subTitle: subTitle,
      buttons: [buttonMsg]
    }).present();
  }
}
